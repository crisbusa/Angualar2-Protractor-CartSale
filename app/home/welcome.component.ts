import { Component } from 'angular2/core';
import {PersonFormComponent} from  '../persona/person-form.component';

@Component({
    templateUrl: 'app/home/welcome.component.html'
})
export class WelcomeComponent {
    public pageTitle: string = "Bienvenidos Pruebas Funcionales Angular2 & Protractor (BDD)";
}