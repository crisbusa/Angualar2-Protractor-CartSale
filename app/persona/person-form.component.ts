import { Component, OnInit } from 'angular2/core';
import { RouteParams, Router } from 'angular2/router';

export class Person {
	constructor(
		public name: string,
		public favoriteColor: string
	){}
}

@Component({
	selector: 'person-form',
	templateUrl: 'app/persona/person-form.component.html'
})

export class PersonFormComponent {
	
	model = new Person('','');
	modelFilled = false;
	active = true;
	submitted = false;
	nombre = 'John';
	message = '';

	sayHello() {
		this.message = 'Yo ' + this.nombre;
	}

	 add(a: any, b: any) {
		if(a && b) {
		return (parseInt(a) + parseInt(b));
		}
	}

	get responseMessage() {	

	return "Mi  Nombre  es  " + this.model.name + " y mi Color Favorito  " + this.model.favoriteColor;
	}
	
	onSubmit(name: string , favColor :string): void { 
		 this.newPerson(name, favColor);
	}

	get diagnostic() {
		return JSON.stringify(this.model)
	}


	newPerson(name:string, color:string) {
		this.model = new Person(name, color);
		this.modelFilled = true;
		this.active = false;
		name=" ";
		setTimeout(() => this.active = true, 0);
		return this.model;
	}
}