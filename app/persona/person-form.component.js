System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var Person, PersonFormComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            Person = (function () {
                function Person(name, favoriteColor) {
                    this.name = name;
                    this.favoriteColor = favoriteColor;
                }
                return Person;
            }());
            exports_1("Person", Person);
            PersonFormComponent = (function () {
                function PersonFormComponent() {
                    this.model = new Person('', '');
                    this.modelFilled = false;
                    this.active = true;
                    this.submitted = false;
                    this.nombre = 'John';
                    this.message = '';
                }
                PersonFormComponent.prototype.sayHello = function () {
                    this.message = 'Yo ' + this.nombre;
                };
                PersonFormComponent.prototype.add = function (a, b) {
                    if (a && b) {
                        return (parseInt(a) + parseInt(b));
                    }
                };
                Object.defineProperty(PersonFormComponent.prototype, "responseMessage", {
                    get: function () {
                        return "Mi  Nombre  es  " + this.model.name + " y mi Color Favorito  " + this.model.favoriteColor;
                    },
                    enumerable: true,
                    configurable: true
                });
                PersonFormComponent.prototype.onSubmit = function (name, favColor) {
                    this.newPerson(name, favColor);
                };
                Object.defineProperty(PersonFormComponent.prototype, "diagnostic", {
                    get: function () {
                        return JSON.stringify(this.model);
                    },
                    enumerable: true,
                    configurable: true
                });
                PersonFormComponent.prototype.newPerson = function (name, color) {
                    var _this = this;
                    this.model = new Person(name, color);
                    this.modelFilled = true;
                    this.active = false;
                    name = " ";
                    setTimeout(function () { return _this.active = true; }, 0);
                    return this.model;
                };
                PersonFormComponent = __decorate([
                    core_1.Component({
                        selector: 'person-form',
                        templateUrl: 'app/persona/person-form.component.html'
                    }), 
                    __metadata('design:paramtypes', [])
                ], PersonFormComponent);
                return PersonFormComponent;
            }());
            exports_1("PersonFormComponent", PersonFormComponent);
        }
    }
});
//# sourceMappingURL=person-form.component.js.map